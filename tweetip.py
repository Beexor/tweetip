import tweepy
import netifaces as ni
from datetime import datetime

# Authenticate to Twitter
# REPLACE consumer_token, consumer_secret, key and secret BEFORE RUNNING SCRIPT

auth = tweepy.OAuthHandler(consumer_token, consumer_secret)
auth.set_access_token(key, secret)

# Create API object
api = tweepy.API(auth)

now=datetime.now()
tweet = now.strftime("%d/%m/%Y %H:%M:%S") + "\n\n"

try:
        ni.ifaddresses('eth0')
        ip_eth0 = ni.ifaddresses('eth0')[ni.AF_INET][0]['addr']
        tweet = tweet + "eth0: " + ip_eth0 + "\n"
        pass
except:
        tweet = tweet + "eth0: 0.0.0.0 \n"
        pass

try:
        ni.ifaddresses('wlan0')
	ip_wlan0= ni.ifaddresses('wlan0')[ni.AF_INET][0]['addr']
	tweet = tweet + "wlan0: " + ip_wlan0 + "\n"                                                                                                                                                                                                                 ip_wlan0 = ni.ifaddresses('wlan0')[ni.AF_INET][0]['addr']                                                                                                                                                                                    tweet = tweet +  "wlan0: " + ip_wlan0                                                                                                                                                                                                        pass
except:
        tweet = tweet + "wlan0: 0.0.0.0"
        pass

api.update_status(tweet)
print("**************")
print("*tweet posted*")
print("**************")